export default [
  {
    path: '/system',
    component: 'Layout',
    alwaysShow: true,
    name: 'system',
    meta: { title: '系统管理', icon: 'el-icon-menu', roles: ['sys:manage'] },
    children: [
      {
        path: '/department',
        component: '/system/department/department',
        alwaysShow: false,
        name: 'department',
        meta: {
          title: '机构管理',
          icon: 'el-icon-document',
          roles: ['sys:dept']
        }
      },
      {
        path: '/userList',
        component: '/system/User/UserList',
        alwaysShow: false,
        name: 'userList',
        meta: {
          title: '用户管理',
          icon: 'el-icon-s-custom',
          roles: ['sys:user']
        }
      },
      {
        path: '/roleList',
        component: '/system/Role/RoleList',
        alwaysShow: false,
        name: 'roleList',
        meta: {
          title: '角色管理',
          icon: 'el-icon-s-tools',
          roles: ['sys:role']
        }
      },
      {
        path: '/menuList',
        component: '/system/Menu/MenuList',
        alwaysShow: false,
        name: 'menuList',
        meta: {
          title: '权限管理',
          icon: 'el-icon-document',
          roles: ['sys:menu']
        }
      }
    ]
  },
  {
    path: '/goods',
    component: 'Layout',
    alwaysShow: true,
    name: 'goods',
    meta: { title: '商品管理', icon: 'el-icon-document', roles: ['sys:goods'] },
    children: [
      {
        path: '/goodCategory',
        component: '/goods/goodsCategory/goodsCategoryList',
        alwaysShow: false,
        name: 'goodCategory',
        meta: {
          title: '分类管理',
          icon: 'el-icon-document',
          roles: ['sys:goodsCategory']
        }
      }
    ]
  },
  {
    path: '/systenConfig',
    component: 'Layout',
    alwaysShow: true,
    name: 'systenConfig',
    meta: {
      title: '系统工具',
      icon: 'el-icon-document',
      roles: ['sys:systenConfig']
    },
    children: [
      {
        path: '/document',
        component: '/system/config/systemDocument',
        alwaysShow: false,
        name: 'http://42.193.158.170:8089/swagger-ui/index.html',
        meta: {
          title: '接口文档',
          icon: 'el-icon-document',
          roles: ['sys:document']
        }
      },
      {
        path: '/systemCode',
        component: '/system/config/code',
        alwaysShow: false,
        name: 'systemCode',
        meta: {
          title: '日志管理',
          icon: 'el-icon-document',
          roles: ['sys:systemCode']
        }
      }
    ]
  }
]
