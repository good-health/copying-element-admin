import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 * 不需要权限就可以访问的路由
 */
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'Dashboard',
        meta: { title: '首页', icon: 'dashboard', affix: true }
      }
    ]
  },
  {
    path: 'external-link',
    component: Layout,
    children: [
      {
        path: 'https://www.baidu.com/',
        meta: { title: '外链', icon: 'dashboard', affix: true }
      }
    ]
  }
]

export const asyncRoutes = [
  {
    path: '/system',
    component: Layout,
    alwaysShow: true,
    name: 'system',
    meta: { title: '系统管理', icon: 'el-icon-menu', roles: ['sys:manage'] },
    children: [
      {
        path: '/department',
        // component: '/system/department/department',
        component: () => import('@/views/system/department/department.vue'),
        alwaysShow: false,
        name: 'department',
        meta: {
          title: '机构管理',
          icon: 'el-icon-document',
          roles: ['sys:dept']
        }
      },
      {
        path: '/userList',
        // component: '/system/User/UserList',
        component: () => import('@/views/system/User/UserList.vue'),
        alwaysShow: false,
        name: 'userList',
        meta: {
          title: '用户管理',
          icon: 'el-icon-s-custom',
          roles: ['sys:user']
        }
      },
      {
        path: '/roleList',
        // component: '/system/Role/RoleList',
        component: () => import('@/views/system/Role/RoleList.vue'),
        alwaysShow: false,
        name: 'roleList',
        meta: {
          title: '角色管理',
          icon: 'el-icon-s-tools',
          roles: ['sys:role']
        }
      },
      {
        path: '/menuList',
        // component: '/system/Menu/MenuList',
        component: () => import('@/views/system/Menu/MenuList.vue'),
        alwaysShow: false,
        name: 'menuList',
        meta: {
          title: '权限管理',
          icon: 'el-icon-document',
          roles: ['sys:menu']
        }
      }
    ]
  },
  {
    path: '/goods',
    component: Layout,
    alwaysShow: true,
    name: 'goods',
    meta: { title: '商品管理', icon: 'el-icon-document', roles: ['sys:goods'] },
    children: [
      {
        path: '/goodCategory',
        // component: '/goods/goodsCategory/goodsCategoryList',
        component: () =>
          import('@/views/goods/goodsCategory/goodsCategoryList.vue'),
        alwaysShow: false,
        name: 'goodCategory',
        meta: {
          title: '分类管理',
          icon: 'el-icon-document',
          roles: ['sys:goodsCategory']
        }
      }
    ]
  },
  {
    path: '/systenConfig',
    component: Layout,
    alwaysShow: true,
    name: 'systenConfig',
    meta: {
      title: '系统工具',
      icon: 'el-icon-document',
      roles: ['sys:systenConfig']
    },
    children: [
      {
        path: '/document',
        // component: '/system/config/systemDocument',
        component: () => import('@/views/system/config/systemDocument.vue'),
        alwaysShow: false,
        name: 'http://42.193.158.170:8089/swagger-ui/index.html',
        meta: {
          title: '接口文档',
          icon: 'el-icon-document',
          roles: ['sys:document']
        }
      },
      {
        path: '/systemCode',
        component: () => import('@/views/system/config/code.vue'),
        alwaysShow: false,
        name: 'systemCode',
        meta: {
          title: '日志管理',
          icon: 'el-icon-document',
          roles: ['sys:systemCode']
        }
      }
    ]
  }
]
const createRouter = () =>
  new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
  })

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter () {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
