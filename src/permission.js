import router from './router'
import store from './store'
import { Message } from 'element-ui'
// 浏览器顶部的进度条
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken } from '@/utils/auth' // get token from cookie
import getPageTitle from '@/utils/get-page-title'
// showSpinner：进度环(右上角的绿色小圆环)显示隐藏
NProgress.configure({ showSpinner: false }) // NProgress Configuration
// 白名单,目前只有一个login页面
const whiteList = ['/login', '/auth-redirect'] // no redirect whitelist
router.beforeEach(async(to, from, next) => {
  // start progress bar
  NProgress.start()
  // set page title
  document.title = getPageTitle(to.meta.title)
  // determine whether the user has logged in
  // 从cookies中获取token, 因为刷新页面,vuex中的数据会丢失,所以存在cookies中
  // 当然也可以存在localstorage中
  const hasToken = getToken()
  // 判断是否有token存在
  if (hasToken) {
    // 进入表示已经拥有token
    // 判断是否登录
    if (to.path === '/login') {
      // 是登陆就跳转到首页
      next({ path: '/' })
      NProgress.done() // hack: https://github.com/PanJiaChen/vue-element-admin/pull/2939
    } else {
      // 这里不是登录页,就是证明已经登录了,就从vuex中获取权限
      const hasRoles = store.getters.roles && store.getters.roles.length > 0
      // 如果存在
      if (hasRoles) {
        // 放行
        next()
      } else {
        try {
          // 没有权限,就获取用户信息,拿到权限,存储到vuex里面
          // roles必须是一个数组的形式
          // note: roles must be a object array! such as: ['admin'] or ,['developer','editor']
          const { roles } = await store.dispatch('user/getInfo')
          // generate accessible routes map based on roles
          // 生成路由数据,是一个数组
          const accessRoutes = await store.dispatch('permission/generateRoutes', roles)
          // dynamically add accessible routes
          // 添加到路由
          router.addRoutes(accessRoutes)
          // hack method to ensure that addRoutes is complete
          // set the replace: true, so the navigation will not leave a history record
          next({ ...to, replace: true })
        } catch (error) {
          // remove token and go to login page to re-login
          await store.dispatch('user/resetToken')
          Message.error(error || 'Has Error')
          next(`/login?redirect=${to.path}`)
          NProgress.done()
        }
      }
    }
  } else {
    /* has no token*/
    // 判断是否在白名单中
    if (whiteList.indexOf(to.path) !== -1) {
      // in the free login whitelist, go directly
      next()
    } else {
      // 跳转登录
      // other pages that do not have permission to access are redirected to the login page.
      next(`/login?redirect=${to.path}`)
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
