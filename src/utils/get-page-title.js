import defaultSettings from '@/settings'

const title = defaultSettings.title || '健康的管理系统'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle}`
  }
  return `${title}`
}
