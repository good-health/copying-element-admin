//判断是否是末级
// 整个循环的目的就是为了给数据添加一个open的属性，
// 为了后面的回显使用，就是根据这个不是最后一级这个属性，来选择
// 设置节点的选中，这样可以防止选中父级节点，因为一旦选中，就会
// 导致它下面的子级被全部选中
// 这个写法是vue3中经常使用的写法，把整个文件作为一个函数导出去
// 然后在具体的页面解构这个函数，得到具体的方法
export default function leafUtils () {
  const setLeaf = arr => {
    if (arr && arr.length > 0) {
      for (let i = 0; i < arr.length; i++) {
        // children>0，就证明它不是末级
        // 其实所有的数据都拥有children这个属性
        if (arr[i].children && arr[i].children.length > 0) {
          // 给非子级的节点设置open属性为false
          arr[i].open = false
          setLeaf(arr[i].children)
        } else {
          // 给最末级的节点设置open属性为true
          arr[i].open = true
        }
      }
    }
    return arr
  }
  return {
    setLeaf
  }
}
