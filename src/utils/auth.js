import Cookies from 'js-cookie'
// js-cookie是一个操作cookie的插件，可以很方便的操作coookie
// 具体的参考地址：https://juejin.cn/post/7222602874631159865
const TokenKey = 'Admin-Token'
const timeKey = 'expireTime'

export function getToken () {
  return Cookies.get(TokenKey)
}

export function setToken (token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken () {
  return Cookies.remove(TokenKey)
}
//清空sessionStorage
export function clearStorage () {
  return sessionStorage.clear()
}

//设置token过期时间
export function setTokenTime (time) {
  return sessionStorage.setItem(timeKey, time)
}
//清空token过期时间
export function removeTokenTime () {
  return sessionStorage.setItem(timeKey, 0)
}
//获取token时间
export function getTokenTime () {
  return sessionStorage.getItem(timeKey)
}
