import axios from 'axios'
import { MessageBox, Message } from 'element-ui'
import store from '@/store'
import {
  getToken,
  getTokenTime,
  setTokenTime,
  removeTokenTime,
  setToken,
  clearStorage,
  removeToken
} from '@/utils/auth'
import { refreshTokenApi } from '@/api/user'
import qs from 'qs'
// create an axios instance 创建访问的axios实例
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // 使用环境变量
  // baseURL: 'http://119.45.133.128:8098/', // url = base url + request url
  // baseURL: 'http://localhost:8089/', // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000 // request timeout
})
// 刷新token
// 注意很奇怪，这里直接把请求放在了外面，没有放在下面的循环内部
function reFresh () {
  let parm = {
    token: getToken()
  }
  // 注意声明函数的时候，都会声明它的返回值，如果不返回，这函数的结果
  // 后面调用的函数都没有办法执行
  return refreshTokenApi(parm).then(res => res)
}
// request interceptor 请求拦截器
let isRefresh = false
service.interceptors.request.use(
  config => {
    // do something before request is sent
    // 获取当前系统时间
    let curent = new Date().getTime()
    // 获取token过期时间
    let expireTime = getTokenTime()
    // 如果token的过期时间大于0 ,证明是有用的过期时间
    if (expireTime > 0) {
      // 除以1000是因为这个是毫秒,需要处理成秒
      let minMax = (expireTime - curent) / 1000 / 60
      // 如果token离过期还有10分钟,刷新token
      if (minMax < 10) {
        // 如果为真
        if (!isRefresh) {
          // 进入到事件内部,把判断值改为true
          // 如果这个判断值本身就是true,他就不会进入到这个判断中
          // 这里是防止，如果有多个请求，就是同时有好几个接口请求发送过来了
          // 这时，这里就可以判定，只有第一个发出的请求，才会进入到这个判断中
          // 也就是说，只有第一个请求时，刷新一下token就可以了
          isRefresh = true
          return reFresh()
            .then(res => {
              if (res.code === 200) {
                // 设置新的token和事件
                setToken(res.data.token)
                setTokenTime(res.data.expireTime)
                // 把新的token添加到头部
                config.headers.token = getToken()
              }
              return config
            })
            .catch(res => {
              console.log(res)
            })
            .finally(() => {
              // 这个是不管对错,最终都会进入到这里
              isRefresh = false
            })
        }
      }
    }
    if (store.getters.token) {
      // let each request carry token
      // ['X-Token'] is a custom headers key
      // please modify it according to the actual situation
      // config.headers['X-Token'] = getToken()
      config.headers['token'] = getToken()
    }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor 响应拦截器
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
   */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    const res = response.data
    // console.log('返回的验证码信息', res)
    // console.log('返回的验证码信息', response)
    // if the custom code is not 20000, it is judged as an error.
    if (res.code !== 200) {
      // 验证码处理,因为验证码接口的response.data中没有code,所以需要单独做一下处理
      // 返回的是arraybuffer,需要转化成base64格式的处理
      // 1.把arraybuffer转化成二进制字符
      // 2.把二进制字符转化成base64 (btoa方法)字符给image使用
      let indexs = response.config.responseType
      if (indexs === 'arraybuffer') {
        // 这里Promise.resolve()应该可以省略不写，至二级返回这个方法的结果应该就可以
        // 注意这个返回指的是致辞请求结果的返回，首先判断返回的状态码不对，然后在这些异常的状态
        // 返回值中，判断如果类型是arraybuffer,就把返回的结果进行转化
        return Promise.resolve(
          'data:image/png;base64,' +
          btoa(
            new Uint8Array(res).reduce(
              // reduce的第一个参数是一个函数，第二个参数就是传递给函数的初始值，是可选的
              // 函数的第一个参数，是必须的，是初始值，也是计算后需要返回的值
              // 函数的第二个参数也是必须的，是当前元素，就是目前正在被循环作用的那个元素
              (data, byte) => data + String.fromCharCode(byte),
              ''
            )
          )
        )
      }
      Message({
        message: res.msg || '服务器出错',
        type: 'error',
        duration: 5 * 1000
      })

      // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
      if (res.code === 600 || res.code === 50012 || res.code === 50014) {
        // to re-login
        MessageBox.confirm('用户登录信息过期,请重新登录', '系统登录', {
          confirmButtonText: '登录',
          cancelButtonText: '取消',
          type: 'warning'
        }).then(() => {
          store.dispatch('user/resetToken').then(() => {
            // 清空token
            removeToken()
            clearStorage()
            removeTokenTime()
            location.reload()
          })
        })
      }
      return Promise.reject(new Error(res.msg || '服务器出错'))
    } else {
      return res
    }
  },
  error => {
    console.log('err' + error) // for debug
    Message({
      message: error.msg,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

// 请求方法
const http = {
  post (url, params) {
    return service.post(url, params, {
      transformRequest: [
        params => {
          return JSON.stringify(params)
        }
      ],
      headers: {
        'Content-Type': 'application/json'
      }
    })
  },
  put (url, params) {
    return service.put(url, params, {
      transformRequest: [
        params => {
          return JSON.stringify(params)
        }
      ],
      headers: {
        'Content-Type': 'application/json'
      }
    })
  },
  // 这里有Api和RestApi的区别，这些区别就是地址的呈现方式不一样
  // params => {id: 10} get请求会通过qs把这个参数序列化
  // http://localhost:9527/#/user?id=10
  get (url, params) {
    return service.get(url, {
      params: params,
      paramsSerializer: params => {
        return qs.stringify(params)
      }
    })
  },
  // params => {id: 10}
  // http://localhost:9527/#/user/10
  // 这里的处理方式其实就是取出请求参数的值，忽略他的属性名
  // 然后在获取的属性值这里使用/分开，然后删除最后一个斜杠，这样可以符合restApi的风格
  getRestApi (url, params) {
    let _params
    if (Object.is(params, undefined || null)) {
      _params = ''
    } else {
      _params = '/'
      for (const key in params) {
        // console.log(key)
        // console.log(params[key])
        // eslint-disable-next-line no-prototype-builtins
        if (
          params.hasOwnProperty(key) &&
          params[key] !== null &&
          params[key] !== ''
        ) {
          _params += `${params[key]}/`
        }
      }
      // 去掉参数最后一位?
      _params = _params.substr(0, _params.length - 1)
    }
    // console.log(_params)
    if (_params) {
      return service.get(`${url}${_params}`)
    } else {
      return service.get(url)
    }
  },
  delete (url, params) {
    let _params
    if (Object.is(params, undefined || null)) {
      _params = ''
    } else {
      _params = '/'
      for (const key in params) {
        // eslint-disable-next-line no-prototype-builtins
        if (
          params.hasOwnProperty(key) &&
          params[key] !== null &&
          params[key] !== ''
        ) {
          _params += `${params[key]}/`
        }
      }
      // 去掉参数最后一位?
      _params = _params.substr(0, _params.length - 1)
    }
    if (_params) {
      return service.delete(`${url}${_params}`).catch(err => {
        Message.error(err.msg)
        return Promise.reject(err)
      })
    } else {
      return service.delete(url).catch(err => {
        Message.error(err.msg)
        return Promise.reject(err)
      })
    }
  },
  upload (url, params) {
    return service.post(url, params, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  },
  login (url, params) {
    return service.post(url, params, {
      transformRequest: [
        params => {
          return qs.stringify(params)
        }
      ],
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    })
  },
  getImage (url) {
    return service.post(url, null, {
      responseType: 'arraybuffer'
    })
  }
}
export default http
