module.exports = {
  // 是否使用分号结尾
  semi: false,
  // 使用单引号
  singleQuote: true,
  // 关闭行尾使用逗号
  trailingComma: "none",
  // 标签闭合位置不单独起一行
  bracketSameLine: true,
  // jsx中标签闭合位置不单独起一行
  jsxBracketSameLine: true,
  htmlWhitespaceSensitivity: "ignore",
  // printWidth: 120
}